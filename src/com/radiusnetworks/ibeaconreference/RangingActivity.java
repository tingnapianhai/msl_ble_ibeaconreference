package com.radiusnetworks.ibeaconreference;

import java.math.BigDecimal;
import java.util.Collection;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.RangeNotifier;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.EditText;

public class RangingActivity extends Activity implements IBeaconConsumer {
	protected static final String TAG = "RangingActivity";
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
	
	private static long time1 = System.currentTimeMillis();
	private static long time2 = System.currentTimeMillis();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ranging);

		iBeaconManager.bind(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		iBeaconManager.unBind(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (iBeaconManager.isBound(this))
			iBeaconManager.setBackgroundMode(this, true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (iBeaconManager.isBound(this))
			iBeaconManager.setBackgroundMode(this, false);
	}

	/*
	double aa = 0;
	BigDecimal a0 = new BigDecimal(aa);
	double m0 = a0.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	*/
	
	@Override
	public void onIBeaconServiceConnect() {
		iBeaconManager.setRangeNotifier(new RangeNotifier() {
			
			@Override
			public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {
				
				time2 = System.currentTimeMillis();
				String dif_time = "" + (time2-time1);
				time1 = time2;
				
				if (iBeacons.size() > 0) {
					//EditText editText = (EditText) RangingActivity.this.findViewById(R.id.rangingText);
					//logToDisplay("First iBeacon: " + iBeacons.iterator().next().getAccuracy() + " m");
					//iBeacons.iterator().next().getProximityUuid();
					//Log.v(TAG, "--- " + iBeacons.size() + " + " + iBeacons.iterator().next());
					}
				else {
					Log.v(TAG, "none--" + dif_time );
					logToDisplay("none--" + dif_time);
					}
				
			if (iBeacons.size() > 0)
				for(IBeacon ibea: iBeacons) {
					
					BigDecimal a0 = new BigDecimal(ibea.getAccuracy());
					double acc = a0.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					String info = dif_time + " "+iBeacons.size() + " Minor:" + ibea.getMinor() +  " Rssi:" + ibea.getRssi() + " meter:" + acc + "\n";
					
					Log.v(TAG, "--- " + info );
					logToDisplay(""+ibea.getProximityUuid());
					logToDisplay(info);
				}
				
			}
			
		});

		try {
			iBeaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
			} 
		catch (RemoteException e) {	}
	}

	private void logToDisplay(final String line) {
		runOnUiThread(new Runnable() {
			public void run() {
				EditText editText = (EditText) RangingActivity.this.findViewById(R.id.rangingText);
				editText.append(line + "\n");
			}
		});
	}
}
