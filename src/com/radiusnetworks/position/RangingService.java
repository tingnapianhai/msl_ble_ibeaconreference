package com.radiusnetworks.position;

import java.math.BigDecimal;
import java.util.Collection;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.RangeNotifier;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.app.Service;
import android.content.Intent;

public class RangingService extends Service implements IBeaconConsumer {
	protected final String TAG = "RangingService";
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
	
	private Handler handler;
	private Message msg;

	private static long time1 = System.currentTimeMillis();
	private static long time2 = System.currentTimeMillis();

	public RangingService() {
		super();
		this.handler = MainActivity.ibeacon_handler;
		Log.v(TAG, "rangingservice Constructor");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		iBeaconManager.bind(this);
		Log.v(TAG, "ranging service onCreate started");
	}

	@Override
	public void onIBeaconServiceConnect() {
		iBeaconManager.setRangeNotifier(new RangeNotifier() {

			@Override
			public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {

				Log.v(TAG, "@@@@@@@@@@@@@@@ ");
				time2 = System.currentTimeMillis();
				String dif_time = "" + (time2 - time1);
				time1 = time2;

				if (iBeacons.size() > 0) {
					// iBeacons.iterator().next().getProximityUuid();
					for (IBeacon ibea : iBeacons) {
						
						// ! verify/check MSL-iBeacons;
						if(!ibea.getProximityUuid().equalsIgnoreCase(Config.iBeacon_UUID) || ibea.getMinor()<86 || ibea.getMinor()>90 ) {
							Log.v(TAG, "non-MSL iBeacon exists " + dif_time);
							continue;
						}

						BigDecimal a0 = new BigDecimal(ibea.getAccuracy());
						double accuracy = a0.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						String info = dif_time + " " + iBeacons.size() + " Minor:" + ibea.getMinor() + " Rssi:"	+ ibea.getRssi() + " meter:" + accuracy + "\n";
						
						setIBeaconRssi(ibea.getMinor(), ibea.getRssi(), accuracy);
						
						msg = new Message();
						msg.what = 0;
						msg.obj = ibea.getMinor()-86;
						handler.sendMessage(msg);
						
						Log.v(TAG, ibea.getProximityUuid()+"--- " + info);
					}//end for
					msg = new Message();
					msg.what = 1;
					handler.sendMessage(msg);
				}//end if
				else {
					Log.v(TAG, "none--" + dif_time);
				}
			}
		});

		try {
			iBeaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
			} 
		catch (RemoteException e) {	}
	}
	
	private void setIBeaconRssi(int minor, int rssi, double accuracy) {
		//verify if this ibeacon is msl-ibeacon;
		if(minor<86 || minor>90)
			return;
		
		int num = minor-86;
		Config.rssi[num] = rssi;
		Config.accuracy[num] = accuracy;
		
		renewRssiSamples(Config.rssiArray[num], rssi);
	}
	
	//往array里面插入元素
	private void renewRssiSamples(int[] arr, int rssi) {
    	for(int i=arr.length-1;i>0;i--) {
    		arr[i] = arr[i-1];
    	}
    	arr[0] = rssi;
    }

	private void logToDisplay(final String line) {
		new Thread(new Runnable() {
			public void run() {
				// TODO
				String str = line + "\n";
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		iBeaconManager.unBind(this);
		Log.v(TAG, "ranging service onDestroy :(((((");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
