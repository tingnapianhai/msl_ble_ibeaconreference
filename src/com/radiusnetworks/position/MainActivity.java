/*
 * test for RssiSignal-Strength to differentiate areas..., created by Kai, 2013-12-19
 * */

package com.radiusnetworks.position;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.radiusnetworks.ibeaconreference.MonitoringActivity;
import com.radiusnetworks.ibeaconreference.R;
import com.radiusnetworks.ibeaconreference.RangingActivity;

public class MainActivity extends Activity {
	
	private Button monitoring;
	private Button ranging;
	static Button area0;
	static Button area1;
	static Button area2;
	static Button area3;
	static Button area4;
	static TextView tv;
	
	static Button AAA0;
	static Button AAA1;
	static Button AAA2;
	static Button AAA3;
	private Context con;
	public static final int EDIT_PREFS = 100;

	static Handler ibeacon_handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0: 
				int va = (Integer) msg.obj;
				setIBeaconValue(va);//TODO
				break;
			case 1: 
				getIBeaconArea();//TODO
				break;
			default:break;
			}
			super.handleMessage(msg);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ibeacon_main);
		preferenceInitialize();
		con = this;
		
		monitoring = (Button)findViewById(R.id.ibeacon_monitoring);
		ranging = (Button)findViewById(R.id.ibeacon_ranging);
		area0 = (Button)findViewById(R.id.ibeacon_area0);
		area1 = (Button)findViewById(R.id.ibeacon_area1);
		area2 = (Button)findViewById(R.id.ibeacon_area2);
		area3 = (Button)findViewById(R.id.ibeacon_area3);
		area4 = (Button)findViewById(R.id.ibeacon_area4);
		tv = (TextView)findViewById(R.id.ibeacon_tv);
		AAA0 = (Button)findViewById(R.id.ibeacon_AAA0);
		AAA1 = (Button)findViewById(R.id.ibeacon_AAA1);
		AAA2 = (Button)findViewById(R.id.ibeacon_AAA2);
		AAA3 = (Button)findViewById(R.id.ibeacon_AAA3);
		
		setAreaColor(-1);
		
		monitoring.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(MainActivity.this, MonitoringActivity.class);
				startActivity(i);
				}
			});
		
		ranging.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(con, RangingActivity.class);
				startActivity(i);
			}
		});
		
		area0.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Intent intent = new Intent(MainActivity.this, RangingService.class);
				//startService(intent);
				startService(new Intent(MainActivity.this, RangingService.class));
			}
		});

		area1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stopService(new Intent(MainActivity.this, RangingService.class));
			}
		});
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
	} //onCreate
	
	//*********************************************
	private void setColor(int number) {
    	switch (number) {
    	case 0: 
    		area0.setBackgroundColor(Color.GREEN);
    		break;
    	case 1: 
    		area0.setBackgroundColor(Color.YELLOW);
    		break;
    	case 2: 
    		area0.setBackgroundColor(Color.BLUE);
    		break;
    	case 3: 
    		area0.setBackgroundColor(Color.RED);
    		break;
    	default: 
    		area0.setBackgroundColor(Color.GRAY);
    		break;
    	}
    }
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void finish() {
		super.finish();
	}
	//*********************************************

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pref:
			Intent intent = new Intent(MainActivity.this, MenuSettingPreferences.class);
			startActivityForResult(intent, EDIT_PREFS);
			return true;
		}
		return false;
	}
	
	private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			Config.iBeacon_UUID = p.getString("pref_ibeacon_uuid", ""+Config.iBeacon_UUID);
	        Config.iBeacon_Major = Integer.parseInt(p.getString("pref_ibeacon_major", ""+Config.iBeacon_Major));
	        Config.iBeacon_TxPower = Integer.parseInt(p.getString("pref_ibeacon_txpower", ""+Config.iBeacon_TxPower));
	        Config.iBeacon_Minors[0] = Integer.parseInt(p.getString("pref_ibeacon_minor0", ""+Config.iBeacon_Minors[0]));
	        Config.iBeacon_Minors[1] = Integer.parseInt(p.getString("pref_ibeacon_minor1", ""+Config.iBeacon_Minors[1]));
	        Config.iBeacon_Minors[2] = Integer.parseInt(p.getString("pref_ibeacon_minor2", ""+Config.iBeacon_Minors[2]));
	        Config.iBeacon_Minors[3] = Integer.parseInt(p.getString("pref_ibeacon_minor3", ""+Config.iBeacon_Minors[3]));
	        Config.iBeacon_Minors[4] = Integer.parseInt(p.getString("pref_ibeacon_minor4", ""+Config.iBeacon_Minors[4]));
			} 
		catch (Exception e) {e.printStackTrace();}
	}
	
	//calculate Rssi-Area: 012, 023, 124, 234
	private static void getIBeaconArea() {
		int areaRs[] = {-300,-300,-300,-300};
		areaRs[0] = Config.rssi[0] + Config.rssi[1] + Config.rssi[2];
		areaRs[1] = Config.rssi[0] + Config.rssi[2] + Config.rssi[3];
		areaRs[2] = Config.rssi[1] + Config.rssi[2] + Config.rssi[4];
		areaRs[3] = Config.rssi[2] + Config.rssi[3] + Config.rssi[4];
		/*int areaNO = 0;
		int areaRssi = areaRs[0];
		for(int i=0;i<4;i++) {
			if(areaRssi < areaRs[i]) {
				areaRssi = areaRs[i];
				areaNO = i;
			}
		}*/
		
		int area00 = Config.rssiArray[0][0]+Config.rssiArray[0][1]+Config.rssiArray[0][2] + 
				Config.rssiArray[1][0]+Config.rssiArray[1][1]+Config.rssiArray[1][2];
		int area33 = Config.rssiArray[3][0]+Config.rssiArray[3][1]+Config.rssiArray[3][2] + 
				Config.rssiArray[4][0]+Config.rssiArray[4][1]+Config.rssiArray[4][2];
		int areaNO = 0;
		int areaRssi = area00;
		if(area00<area33)
			areaNO = 3;
		
		tv.setText(""+areaNO);
		setAreaColor(areaNO);
	}
	
	private static void setAreaColor(int num) {
		switch(num) {
			case -1: 
				AAA0.setBackgroundColor(Color.GRAY);
				AAA1.setBackgroundColor(Color.GRAY);
				AAA2.setBackgroundColor(Color.GRAY);
				AAA3.setBackgroundColor(Color.GRAY);
				break;
			case 0: 
				AAA0.setBackgroundColor(Color.GREEN);
				//AAA1.setBackgroundColor(Color.GRAY);
				//AAA2.setBackgroundColor(Color.GRAY);
				AAA3.setBackgroundColor(Color.GRAY);
				break;
			case 1: 
				AAA0.setBackgroundColor(Color.GRAY);
				//AAA1.setBackgroundColor(Color.GREEN);
				//AAA2.setBackgroundColor(Color.GRAY);
				AAA3.setBackgroundColor(Color.GRAY);
				break;
			case 2: 
				AAA0.setBackgroundColor(Color.GRAY);
				//AAA1.setBackgroundColor(Color.GRAY);
				//AAA2.setBackgroundColor(Color.GREEN);
				AAA3.setBackgroundColor(Color.GRAY);
				break;
			case 3: 
				AAA0.setBackgroundColor(Color.GRAY);
				//AAA1.setBackgroundColor(Color.GRAY);
				//AAA2.setBackgroundColor(Color.GRAY);
				AAA3.setBackgroundColor(Color.GREEN);
				break;
			default:
				AAA0.setBackgroundColor(Color.GRAY);
				AAA1.setBackgroundColor(Color.GRAY);
				AAA2.setBackgroundColor(Color.GRAY);
				AAA3.setBackgroundColor(Color.GRAY);
				break;
		}
	}
	
	private static void setIBeaconValue(int num) {
		switch(num) {
			case 0: 
				area0.setText(""+Config.rssi[0]);
				break;
			case 1: 
				area1.setText(""+Config.rssi[1]);
				break;
			case 2: 
				area2.setText(""+Config.rssi[2]);
				break;
			case 3: 
				area3.setText(""+Config.rssi[3]);
				break;
			case 4: 
				area4.setText(""+Config.rssi[4]);
				break;
			default:
				break;
		}
	}

}
