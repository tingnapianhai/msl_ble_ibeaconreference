package com.radiusnetworks.position;

import java.util.HashMap;

import android.bluetooth.BluetoothDevice;

import com.radiusnetworks.ibeacon.IBeacon;

public class Config {
	
	private IBeacon iBeaconDevices[] = {};
	
	static String iBeacon_UUID = "74278bda-b644-4520-8f0c-720eaf059937";
	static int iBeacon_Major = 426;
	static int iBeacon_TxPower = -60;
	static int iBeacon_Minors[] = {86,87,88,89,90};
	
	static int[] rssi = {-100,-100,-100,-100,-100};
	static int[][] rssiArray = {{-100,-100,-100},{-100,-100,-100},{-100,-100,-100},{-100,-100,-100},{-100,-100,-100}};
	static double[] accuracy = {1000,1000,1000,1000,1000};
	
	static HashMap<IBeacon, Integer> rssiMap = new HashMap<IBeacon, Integer>();
	
	//static int rssiConstant = -100;
	//static double accuracyConstant = 1000;
}
