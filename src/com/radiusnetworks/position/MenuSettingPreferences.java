package com.radiusnetworks.position;

import com.radiusnetworks.ibeaconreference.R;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.Preference;
import android.preference.PreferenceManager;

@SuppressLint("NewApi")
public class MenuSettingPreferences extends PreferenceActivity {

	private String TAG = "MenuSettingPreferences";
    @SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
        Config.iBeacon_UUID = p.getString("pref_ibeacon_uuid", ""+Config.iBeacon_UUID);
        Config.iBeacon_Major = Integer.parseInt(p.getString("pref_ibeacon_major", ""+Config.iBeacon_Major));
        Config.iBeacon_TxPower = Integer.parseInt(p.getString("pref_ibeacon_txpower", ""+Config.iBeacon_TxPower));
        Config.iBeacon_Minors[0] = Integer.parseInt(p.getString("pref_ibeacon_minor0", ""+Config.iBeacon_Minors[0]));
        Config.iBeacon_Minors[1] = Integer.parseInt(p.getString("pref_ibeacon_minor1", ""+Config.iBeacon_Minors[1]));
        Config.iBeacon_Minors[2] = Integer.parseInt(p.getString("pref_ibeacon_minor2", ""+Config.iBeacon_Minors[2]));
        Config.iBeacon_Minors[3] = Integer.parseInt(p.getString("pref_ibeacon_minor3", ""+Config.iBeacon_Minors[3]));
        Config.iBeacon_Minors[4] = Integer.parseInt(p.getString("pref_ibeacon_minor4", ""+Config.iBeacon_Minors[4]));
        
        CheckBoxPreference MODEL = (CheckBoxPreference)getPreferenceManager().findPreference("pref_filter_model");
        MODEL.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		//Config.MODEL_DIR = (Boolean) newValue;
        		return true;
        		}
        	});//listener
        
        final EditTextPreference IBEACON_UUID = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_uuid");
        IBEACON_UUID.setSummary(""+Config.iBeacon_UUID);
        IBEACON_UUID.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_UUID = (String)newValue;
        		IBEACON_UUID.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_MAJOR = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_major");
        IBEACON_MAJOR.setSummary(""+Config.iBeacon_Major);
        IBEACON_MAJOR.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Major = Integer.parseInt((String)newValue);
        		IBEACON_MAJOR.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_TXPOWER = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_txpower");
        IBEACON_TXPOWER.setSummary(""+Config.iBeacon_TxPower);
        IBEACON_TXPOWER.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_TxPower = Integer.parseInt((String)newValue);
        		IBEACON_TXPOWER.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        
        
        final EditTextPreference IBEACON_MINOR0 = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_minor0");
        IBEACON_MINOR0.setSummary(""+Config.iBeacon_Minors[0]);
        IBEACON_MINOR0.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Minors[0] = Integer.parseInt((String)newValue);
        		IBEACON_MINOR0.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_MINOR1 = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_minor1");
        IBEACON_MINOR1.setSummary(""+Config.iBeacon_Minors[1]);
        IBEACON_MINOR1.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Minors[1] = Integer.parseInt((String)newValue);
        		IBEACON_MINOR1.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_MINOR2 = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_minor2");
        IBEACON_MINOR2.setSummary(""+Config.iBeacon_Minors[2]);
        IBEACON_MINOR2.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Minors[2] = Integer.parseInt((String)newValue);
        		IBEACON_MINOR2.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_MINOR3 = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_minor3");
        IBEACON_MINOR3.setSummary(""+Config.iBeacon_Minors[3]);
        IBEACON_MINOR3.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Minors[3] = Integer.parseInt((String)newValue);
        		IBEACON_MINOR3.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        final EditTextPreference IBEACON_MINOR4 = (EditTextPreference)getPreferenceManager().findPreference("pref_ibeacon_minor4");
        IBEACON_MINOR4.setSummary(""+Config.iBeacon_Minors[4]);
        IBEACON_MINOR4.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		Config.iBeacon_Minors[4] = Integer.parseInt((String)newValue);
        		IBEACON_MINOR4.setSummary(""+(String)newValue);
        		return true;
        		}
        	});//listener
        
    }
}
    
/*	private static int prefs=R.xml.preferences;
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        } catch (NoSuchMethodException e) { //Api < 11
            AddResourceApiLessThan11();
        }
    }
    protected void AddResourceApiLessThan11()
    {
        addPreferencesFromResource(prefs);
    }

    protected void AddResourceApi11AndGreater()
    {
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PF()).commit();
    }
    public static class PF extends PreferenceFragment
    {       
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(SettingPreferences.prefs); //outer class private members seem to be visible for inner class, and making it static made things so much easier
        }
    }*/


